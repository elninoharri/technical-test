Installation Using Docker

Clone the Repo:

git clone git@gitlab.com:elninoharri/example-app.git
git clone https://gitlab.com/elninoharri/example-app.git


cd example-app
SSH into the Docker container with make ssh and run the following.

composer create-project
php artisan migrate
php artisan db:seed

Exit from Docker container with CTRL+C or exit.

Start the local development server with make up.
Run make to see available commands.
Always ssh into Docker container app by running make ssh before executing any artisan commands.

Installation On Local

Clone the Repo:

git clone git@gitlab.com:elninoharri/example-app.git
git clone https://gitlab.com/elninoharri/example-app.git


cd example-app
On Visual Studio Code Terminal run the following.

composer install
php artisan migrate
php artisan db:seed (optional, leave it blank if you don't want it)
php artisan serve --port=8080

you can also import the database manually
the database is on database/example_app_09-08-22.sql