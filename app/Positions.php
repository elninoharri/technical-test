<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\ServerException;

class Positions
{
    private $client;

    public function __construct($language)
    {
        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://dev3.dansmultipro.co.id/api/recruitment/',
            // You can set any number of default request options.
            'timeout' => 60.0,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept-Language' => $language
            ]
        ]);
    }

    public function getPositions(array $params = [])
    {
        if ( isset($params['description']) || isset($params['location']) ) {
            $response = $this->client->request('GET', 'positions.json', ['query' => $params]);
        }
        else{
            $response = $this->client->request('GET', 'positions.json');
        }
        
        if ($response->getStatusCode() === 200) {

            $data = json_decode($response->getBody());
            return $data;

        } else {
            return null;
        }
    }
}