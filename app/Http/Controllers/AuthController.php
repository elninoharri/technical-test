<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    function loginView()
    {
        return view("login");
    }

    function registerView()
    {
        return view("register");
    }

    function doLogin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',   // required and email format validation
            'password' => 'required', // required and number field validation

        ]); // create the validations
        if ($validator->fails())   //check all validations are fine, if not then redirect and show error messages
        {

            return back()->withInput()->withErrors($validator);
            // validation failed redirect back to form

        } else {
            //validations are passed try login using laravel auth attemp
            if (\Auth::attempt($request->only(["email", "password"]))) {
                return redirect("positions")->with('success', 'Successfully login ...');
            } else {
                return back()->withErrors( "Invalid credentials"); // auth fail redirect with error
            }
        }
    }

    function doRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',   // required and email format validation
            'password' => 'required|min:8', // required and number field validation
            'confirm_password' => 'required|same:password',

        ]); // create the validations
        if ($validator->fails())   //check all validations are fine, if not then redirect and show error messages
        {

            return back()->withInput()->withErrors($validator);
            // validation failed redirect back to form

        } else {
            //validations are passed, save new user in database
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            return redirect("login")->with('success', 'You have successfully registered, Login to access your dashboard');
        }
    }
    
    // show dashboard
    function dashboard()
    {
        if (Auth::check()) {
            return view("dashboard");
        }
        else{
            return redirect("login")->with('error', 'Please login first ...');
        }
    }

    // logout method to clear the sesson of logged in user
    function logout()
    {
        \Auth::logout();
        return redirect("login")->with('success', 'Logout successfully');;
    }

    // show profile
    function profile()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return view("profile", compact('user'));
    }

    function doUpdateProfile(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'dob' => 'required',
            'gender' => 'required',

        ]); // create the validations
        if ($validator->fails())   //check all validations are fine, if not then redirect and show error messages
        {

            return back()->withInput()->withErrors($validator);
            // validation failed redirect back to form

        } else {
            //validations are passed, save new user in database
            $user_id = auth()->user()->id;
            $user = User::find($user_id);
            $user->name = $request->name;
            $user->dob = $request->dob;
            $user->gender = $request->gender;
            $user->address = $request->address;
            
            try {
                $user->save();
                return redirect("dashboard")->with('success', 'You have successfully updated your profile');
            } catch (\Throwable $th) {
                return redirect("dashboard")->with('error', 'Failed to updated your profile'); 
            }
        }
    }

    function password()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return view("password", compact('user'));
    }

    function doUpdatePassword(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8', // required and number field validation
            'confirm_password' => 'required|same:password',
        ]); // create the validations
        if ($validator->fails())   //check all validations are fine, if not then redirect and show error messages
        {

            return back()->withInput()->withErrors($validator);
            // validation failed redirect back to form

        } else {
            //validations are passed, save new user in database
            $user_id = auth()->user()->id;
            $user = User::find($user_id);
            $user->password = bcrypt($request->password);
            try {
                $user->save();
                return redirect("dashboard")->with('success', 'You have successfully updated your password');
            } catch (\Throwable $th) {
                return redirect("dashboard")->with('error', 'Failed to updated your password');
            }
        }
    }

    function forgot()
    {
        return view("forgot");
    }

    function doForgotPassword(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',   // required and email format validation
        ]); // create the validations
        if ($validator->fails())   //check all validations are fine, if not then redirect and show error messages
        {

            return back()->withInput()->withErrors($validator);
            // validation failed redirect back to form

        } else {
            //validations are passed, save new user in database
            $user = User::where('email', $request->email)->first();
            
            if ($user) {
                try {
                    $user->password = bcrypt('default123');
                    $user->save();
                    return redirect("login")->with('success', 'login with default password ==> default123, after login please do not forgot to change your password');
                } catch (\Throwable $th) {
                    return redirect("login")->with('error', 'Failed to forgoted your password');
                }
                
            }
            else{
                return redirect("forgot")->with('error', 'Email has not regitered');
            }
        }
    }
}