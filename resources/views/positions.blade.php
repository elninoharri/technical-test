<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title> Dashboard </title>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}" />
  
  <script src="{{ url('assets/library/bootstrap.min.js') }}" type="text/javascript"
    charset="utf-8"></script>
  <style>
    .error {
      color: red !important
    }
    .dash{
      height: 400px;
      justify-content: center;
      align-items: center;
      font-size: 20px;
      font-weight: bold;
      display: flex;
      color:green;
      flex-direction: column;

    }
  </style>
</head>

<body class="antialiased">
  
    
    <!-- Image and text -->
    <nav class="navbar navbar-light bg-light justify-content-end">
      <li class="nav-item dropdown" style="list-style-type: none;">
        <a class="navbar-brand nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="{{ url('assets/images/default-profile.jpg') }}" width="30" height="30" class="d-inline-block align-top" alt="">
          @if(\Auth::check())
          Welcome, {{\Auth::user()->email}}
          @else
          @endif
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="{{url('dashboard')}}">Dashboard</a>
          <a class="dropdown-item" href="{{url('profile')}}">Update Profile</a>
          <a class="dropdown-item" href="{{url('password')}}">Update Password</a>
          <a class="dropdown-item" href="{{url('positions')}}">Positions Data</a>
          <a class="dropdown-item" href="{{url('logout')}}">Logout</a>
        </div>
      </li>
    </nav>
    <br>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Show any success message -->
                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! \Session::get('success') !!}</li>
                        </ul>
                    </div>
                @endif
                @if (\Session::has('error'))
                    <div class="alert alert-danger">
                        <ul>
                            <li>{!! \Session::get('error') !!}</li>
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="container">
    <form method="get" action="{{route(Route::currentRouteName() ?? '')}}" id="searchFilter">
        <div class="row">
            <div class="col-md-5">
                <div class="form-group" style="margin-top:15px;">
                    <input id="searchByDescription" name="description" class="form-control" data-param="description"
                        placeholder="Masukkan kata kunci deskripsi..." type="text" value="{{request('description')}}"/>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group" style="margin-top:15px;">
                    <input id="searchByLocation" name="location" class="form-control" data-param="location"
                        placeholder="Masukkan kata kunci lokasi..." type="text" value="{{request('location')}}"/>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group" style="margin-top:15px;">
                    <button type="submit" class="btn btn-primary">Filter</button>&nbsp
                    <a href="{{route(Route::currentRouteName() ?? '')}}" class="btn btn-danger">Reset</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="dataTable" style="font-size:12px;" class="dataTable table table-striped dt-responsive nowrap" style="width: 100%">
                        <thead>
                            <tr>
                                <th data-name="type" data-orderable="true">Type</th>
                                <th data-name="url" data-orderable="true">Url</th>
                                <th data-name="created_at" data-orderable="true">created_at</th>
                                <th data-orderable="false">company</th>
                                <th data-orderable="false">company_url</th>
                                <th data-orderable="false">location</th>
                                <th data-orderable="false">Title</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($positions ?? [] as $data)
                                <tr>
                                    <td>{{ $data->type ?? '' }}</td>
                                    <td>{{ $data->url ?? '' }}</td>
                                    <td>{{ $data->created_at ?? '' }}</td>
                                    <td>{{ $data->company ?? '' }}</td>
                                    <td>{{ $data->company_url ?? '' }}</td>
                                    <td>{{ $data->location ?? '' }}</td>
                                    <td>{{ $data->title ?? '' }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    {!! $positions->withQueryString()->render() !!}
                    
                </div>
            </div>
        </div>
    </form>
    </div>
    <!-- credits -->
    <div class="text-center">
      <p>
        <a href="#" target="_top"></a>
      </p>
    </div>
  
    @push('styles')
    <link rel="stylesheet" href="{{ url('assets/css/datatables.min.css')}}"/>
    @endpush
        
</body>
</html>